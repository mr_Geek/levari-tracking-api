/**
 * Hour.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 var moment = require('moment')

module.exports = {

  attributes: {

    clientName: {
      type: 'string',
      defaultsTo: 'Not Mentioned'
    },

    pricePerHour: {
      type: 'string',
      defaultsTo: '10'
    },

    numberOfHours: {
      type: 'string',
      defaultsTo: '0'
    },

    totalPrice: {
      type: 'string',
      defaultsTo: '0'
    },

    user: {
      type: 'string'
    },

    toJSON: function() {
      var obj = this.toObject()
      obj.createdAt = moment(obj.createdAt).format('MM-DD-YYYY hh:mmA')
  		return obj
    }



  }
};
