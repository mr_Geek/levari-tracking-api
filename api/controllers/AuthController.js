/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var jwt = require('jsonwebtoken')
var passport = require('passport')

function _onPassportAuth(req, res, err, user, info) {
	if (err) {
		return res.send(err)
	}
	if (!user) {
		return res.unauthorized(null, info && info.code, info && info.message)
	}
	user.name = user.firstName
	var outgoingToken = jwt.sign({
		user_id: user.id.toString(),
		custom: user
	}, sails.config.jwtSettings.secret)
	var url = req.query.redirect_uri +
						'&token=' + encodeURIComponent(outgoingToken) +
						'&state=' + encodeURIComponent(req.query.state)

	console.log(url)
	return res.redirect(url)
	/*return res.ok({
		token: CipherService.createToken(user)
	})*/
}

function _onPassportCustomAuth(req, res, err, user, info) {
	if (err) {
		return res.send(err)
	}
	if (!user) {
		return res.unauthorized(null, info && info.code, info && info.message)
	}
	return res.ok({
		token: CipherService.createToken(user.toJSON())
	})
}

module.exports = {

	signin: function(req, res) {
		try {

			// Are we authenticating with Ioinc cloud services !?
			if (req.query.redirect_uri && req.query.state && req.query.token) {
				console.log('redirect_uri: ' + req.query.redirect_uri )
				console.log('state: ' + req.query.state )
				console.log('token: ' + req.query.token )
				var incomingToken = jwt.verify(req.query.token, sails.config.jwtSettings.secret, function(err, decoded){
					if (err) {
						console.log(err)
						return res.status(401).send('ERROR')
					}
					req.query.email = decoded.data.email
					req.query.password = decoded.data.password
					/*
					// Need to know what bind() do in order to pass the user object to be authenticated
					*/
					passport.authenticate('local', _onPassportAuth.bind(this, req, res))(req, res)
				})
			} else {
				if (req.param('email') && req.param('password')) {
					passport.authenticate('local', _onPassportCustomAuth.bind(this, req, res))(req, res)
				} else {
					return res.status(406).json({
						message: 'Error, No params were sent..',
						code: 'EAUTH_FEW_PARAMS'
					})
				}
			}



		} catch (ex) {
			console.log('Error in jwt: ' + ex)
			return res.status(401).send('JWT Error')
		}
	},

	signup: function(req, res) {
		var username = req.param('username') ? req.param('username') : null
		var password = req.param('password') ? req.param('password') : null
		var firstName = req.param('firstName') ? req.param('firstName') : null
		var email = req.param('email') ? req.param('email') : null

		if (username && password && firstName && email ) {
			var userModel = {
				provider: "local",
				username: username,
				password: password,
				firstName: firstName,
				email: email
			}

			User.findOne({email: email}).exec(function(err, user){
				if (err){
					return res.json({
						code: 'E_REGISTER_FIND_DB',
						token: false,
						message: 'Error while finding user: ' + err
					})
				}
				if (!user){
					User.create(userModel, function(err, user){
						if (err) {
							return res.json({
								code: 'E_REGISTER_CREATE_DB',
								token: false,
								message: 'Error while creating new user: ' + err
							})
						}
						else {
							return res.json({
								code: 'S_REGISTER_SUCCESS',
								message: 'User created successfully.',
								token: CipherService.createToken(user)
							})
						}
					})
				}
				else{
					return res.json({
						code: 'E_ALREADY_REGISTERD',
						message: 'User already exists',
						token: false
					})
				}
			})
		} else {
			return res.json({
				code: 'E_REGISTER_EMPTY_CRED',
				token: false,
				message: 'Error there are missing credentials'
			})
		}
	},

	refreshToken: function(req, res) {
		var authentication = req.headers.authentication ? req.headers.authentication : null


		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {

					var errorName = err.name ? err.name : err
					errorName = errorName ? 'TokenExpiredError' : err

					if (errorName !== 'TokenExpiredError') {
						return res.json({
							code: 'E_AUTHO_REFSH_TOKEN',
							message: 'Error occurred, While authenticating.',
							token: false
						})
					}
					decoded = jwt.decode(authentication)
					/*check if more than 5 days, force login*/
					if (decoded.iat - new Date() > 5) {
						return res.json({
							code: 'TOKEN_EXP',
							message: 'Token has been expired due to security reasons you have to login again.',
							token: false
						})
					}

					/*Check if user already exists in our DB*/
					User.findOne({email: decoded.email}, function(err, user) {
						if (err) {
							return res.json({
								code: 'E_TOKEN_USER_FIND',
								message: 'Error, while authenticating user.',
								token: false
							})
						}
						if (!user) {
							return res.json({
								code: 'E_USER_NOT_FOUND',
								message: 'There is no user in our database with such email: ' + decoded.email,
								token: false
							})
						} else {
							var newToken = CipherService.createToken(user.toJSON())
							return res.json({
								code: 'REFSHD_TOKEN',
								message: 'Token has been refreshed, you are now authenticated.',
								token: newToken
							})
						}
					})


				})
			} catch (e) {
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					token: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				token: false
			})
		}
	}

};
